import com.codeborne.selenide.Configuration;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
//import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;

public class Task1 {

    @BeforeClass
    public static void setup() {

        Configuration.browser = "chrome";
        System.setProperty("webdriver.chrome.driver", "c:/Users/marchuk.m/chromeDr/chromedriver.exe");

        open("https://www.google.com/search");
        Configuration.startMaximized=true;
    }

    @Test
    public void task1() {
        $(By.id("lst-ib")).click();
        $(By.id("lst-ib")).setValue("wezom");

        $(By.xpath("//*[@id=\"sbtc\"]/div[2]/div[2]/div[1]/div/ul/li[11]/div/span[1]/span/input")).click();
        $(By.xpath("//*[@id=\"rso\"]/div[1]/div/div/div/div/h3/a")).shouldHave(text("wezom"));
    }
}
